import java.util.Random;

public class Board {
	private int width;
	private int height;
	private int mineCount;
	private String level;

	public char flag = '⚑';
	public char mine = '✸';

	private char[][] board;

	public Board(String level, int rows, int columns, int mines) {
		setLevel(level, rows, columns, mines);
		reset();
	}

	public int getWidth() {
		return this.width;
	}

	public int getHeight() {
		return this.height;
	}

	public String getLevel() {
		return this.level;
	}

	public void setLevel(String level, int rows, int columns, int mines) {
		this.level = level.toUpperCase();
		if (level.equals("EASY")) {
			this.mineCount = 10;
			this.width = 9;
			this.height = 9;
		} else if (level.equals("MEDIUM")) {
			this.mineCount = 40;
			this.width = 16;
			this.height = 16;
		} else if (level.equals("HARD")) {
			this.mineCount = 99;
			this.width = 32;
			this.height = 16;
		} else {
			System.out.println("Invalid level selected. Set to easy.");
			this.level = "EASY";
			this.mineCount = 10;
			this.width = 9;
			this.height = 9;
		}
	}

	// Returns the number of mines surrounding the position
	public int nearbyMines(int row, int col) {
		int minesNearby = 0;
		for (int i = row - 1; i <= row + 1; i++) {
			for (int j = col - 1; j <= col + 1; j++) {
				if (isPiece(i, j, this.mine)) {
					minesNearby++;
				}
			}
		}
		return minesNearby;
	}

	// Will return true if the position is equal to 'piece'
	public boolean isPiece(int row, int col, char piece) {
		if (isValid(row, col) && this.board[row][col] == piece) {
			return true;
		} else {
			return false;
		}
	}

	// return true if in the bounds
	public boolean isValid(int row, int col) {
		if (row >= 0 && row < this.height && col >= 0 && col < this.width) {
			return true;
		} else {
			return false;
		}
	}

	// Returns a string version of the piece at the position
	public String getPieceAt(int row, int col) {
		return Character.toString(this.board[row][col]);
	}

	public void reset() {
		board = new char[this.height][this.width];

		// Generate mines
		int[] mineIndexes = new Random().ints(0, (this.width * this.height - 1)).limit(mineCount).distinct().toArray();

		// Fill the board with the mines
		for (int i = 0; i < mineIndexes.length; i++) {
			this.board[Math.floorDiv(mineIndexes[i], this.width)][mineIndexes[i] % this.width] = this.mine;
		}
		// Add the numbers
		for (int i = 0; i < this.board.length; i++) {
			for (int j = 0; j < this.board[0].length; j++) {
				if (this.board[i][j] != mine) {
					int minesNearby = nearbyMines(i, j);
					if (minesNearby != 0) {
						this.board[i][j] = (char) (minesNearby + 48);
					} else {
						this.board[i][j] = ' ';
					}
				}
			}
		}
	}

	public char getMine() {
		return this.mine;
	}

	public String toString() {
		String newString = "";

		for (int i = 0; i < this.board.length; i++) {
			for (int j = 0; j < this.board[0].length; j++) {
				newString += this.board[i][j];
			}
			newString += "\n";
		}
		return newString;
	}
}
