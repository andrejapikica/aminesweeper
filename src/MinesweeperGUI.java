import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.time.Duration;
import java.time.LocalDateTime;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

public class MinesweeperGUI extends JFrame {


	static int rows;
	static int columns;
	static int mines;

	//timer
	private Timer timer;
	private LocalDateTime startTime;
	private Duration duration = Duration.ofMinutes(0);

	Board board = new Board("", 0, 0, 0);

	JButton[][] boardButtons;
	JButton resetButton = new JButton("");

	JFrame frame = new JFrame("Minesweeper");

	JMenuItem easyOption = new JMenuItem("Easy");
	JMenuItem mediumOption = new JMenuItem("Medium");
	JMenuItem hardOption = new JMenuItem("Hard");

	boolean gameOver = false;

	String flag = Character.toString(board.flag);

	Color background = new Color(236, 236, 236);
	Color defaultBg = new JButton().getBackground();

	public MinesweeperGUI(String level, int rows, int columns, int mines) {
		frame.setJMenuBar(createMenuBar());

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setContentPane(createBoard(board.getLevel(), rows, columns, mines));
		frame.setVisible(true);
		frame.setResizable(true);
	}


	public JMenuBar createMenuBar() {
		JMenuBar menuBar;
		JMenu menu;

		// Initalise the menu bar
		menuBar = new JMenuBar();
		menu = new JMenu("File");

		frame.add(menu);

		menuBar.add(menu);

		menu.add(easyOption);
		easyOption.addActionListener(new MyActionListener());

		menu.add(mediumOption);
		mediumOption.addActionListener(new MyActionListener());

		menu.add(hardOption);
		hardOption.addActionListener(new MyActionListener());

		return menuBar;
	}

	public JPanel createBoard(String level, int rows, int columns, int mines) {
		board.setLevel(level, rows, columns, mines);
		board.reset();

		gameOver = false;

		JPanel mainPanel = new JPanel(new BorderLayout());
		JPanel gameBoard = new JPanel(new GridLayout(board.getHeight(), board.getWidth()));

		boardButtons = new JButton[board.getHeight()][board.getWidth()];

		frame.add(mainPanel);

		// switch between level
		switch (level) {
		case "EASY":
			frame.setSize(500, 600);
			gameBoard.setPreferredSize(new Dimension(450, 450));
			break;
		case "MEDIUM":
			frame.setSize(900, 1000);
			gameBoard.setPreferredSize(new Dimension(800, 800));
			break;
		case "HARD":
			frame.setSize(1800, 1000);
			gameBoard.setPreferredSize(new Dimension(1600, 800));
			break;
		}

		resetButton.setIcon(new ImageIcon(Class.class.getResource("/resources/sun.jpg")));

		timer = new Timer(500, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				getTimer();
			}

		});
		startTime = LocalDateTime.now();
		timer.start();
		resetButton.setText("Start");
		mainPanel.add(resetButton);
		mainPanel.add(gameBoard, BorderLayout.SOUTH);

		resetButton.addActionListener(new MyActionListener());

		// Initialise all the buttons
		for (int i = 0; i < board.getHeight(); i++) {
			for (int j = 0; j < board.getWidth(); j++) {
				boardButtons[i][j] = new JButton();
				boardButtons[i][j].setVisible(true);
				setColour(i, j);

				gameBoard.add(boardButtons[i][j]);
				boardButtons[i][j].addMouseListener(new MyMouseListener());
				boardButtons[i][j].addActionListener(new MyActionListener());
				boardButtons[i][j].setFont(new Font("Arial Unicode MS", Font.BOLD, 20));
			}
		}

		return mainPanel;
	}

	private void getTimer() {
		LocalDateTime now = LocalDateTime.now();
		Duration runningTime = Duration.between(startTime, now);
		Duration timeLeft = runningTime.minus(duration);
		if (timeLeft.isZero() || timeLeft.isNegative()) {
			timeLeft = Duration.ZERO;
			resetButton.doClick(); // Cheat
		}

		resetButton.setText(format(timeLeft));
	}

	protected String format(Duration duration) {
		long hours = duration.toHours();
		long mins = duration.minusHours(hours).toMinutes();
		long seconds = duration.minusMinutes(mins).toMillis() / 1000;
		return String.format("%02dh %02dm %02ds", hours, mins, seconds);
	}
	// Set color for number
	public void setColour(int row, int col) {
		String currentPiece = board.getPieceAt(row, col);
		switch (currentPiece) {
		case "1":
			boardButtons[row][col].setForeground(new Color(1, 0, 254));
			break;
		case "2":
			boardButtons[row][col].setForeground(new Color(1, 127, 1));
			break;
		case "3":
			boardButtons[row][col].setForeground(new Color(254, 0, 0));
			break;
		case "4":
			boardButtons[row][col].setForeground(new Color(0, 0, 127));
			break;
		case "5":
			boardButtons[row][col].setForeground(new Color(129, 1, 2));
			break;
		case "6":
			boardButtons[row][col].setForeground(new Color(0, 128, 129));
			break;
		case "7":
			boardButtons[row][col].setForeground(new Color(0, 0, 0));
			break;
		case "8":
			boardButtons[row][col].setForeground(new Color(128, 128, 128));
			break;
		}
	}

	//get all row & column
	public void reveal(int row, int col) {
		if (board.isPiece(row, col, ' ')) {
			revealBlanks(row, col);
		} else {
			if (board.isValid(row, col) && !board.isPiece(row, col, board.getMine())) {
				boardButtons[row][col].setText(board.getPieceAt(row, col));
				boardButtons[row][col].setBackground(background);
				setColour(row, col);
			}
		}
	}

	//get all the mines on the board
	public void revealMines() {
		for (int i = 0; i < boardButtons.length; i++) {
			for (int j = 0; j < boardButtons[0].length; j++) {
				if (board.isPiece(i, j, board.mine)) {
					if (boardButtons[i][j].getText().equals(flag) == false) {
						boardButtons[i][j].setForeground(Color.BLACK);
						boardButtons[i][j].setText(board.getPieceAt(i, j));
						timer.stop();
					} else if (board.isPiece(i, j, board.mine) && boardButtons[i][j].getText().equals(flag)) {
						boardButtons[i][j].setForeground(Color.BLACK);
						boardButtons[i][j].setBackground(Color.RED);
					}
				}
			}
		}
	}

	// get all blank places
	public void revealBlanks(int row, int col) {
		if (!boardButtons[row][col].getBackground().equals(defaultBg)) {
			return;
		}
		if (board.isPiece(row, col, ' ')) {
			boardButtons[row][col].setBackground(background);
			for (int i = row - 1; i <= row + 1; i++) {
				for (int j = col - 1; j <= col + 1; j++) {
					if (board.isPiece(i, j, ' ')) {
						revealBlanks(i, j);
					} else {
						reveal(i, j);
					}
				}
			}
		}
	}

	public boolean isWinner() {
		for (int i = 0; i < boardButtons.length; i++) {
			for (int j = 0; j < boardButtons[0].length; j++) {
				if (!board.isPiece(i, j, board.mine) && boardButtons[i][j].getBackground().equals(defaultBg)) {
					return false;
				}
			}
		}
		return true;
	}

	public void setFlag(int row, int col) {
		if (!boardButtons[row][col].getText().equals(flag)) {
			boardButtons[row][col].setText(flag);
			boardButtons[row][col].setForeground(Color.RED);
		} else {
			boardButtons[row][col].setText("");
		}
	}

	public class MyActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			//action on level menu
			if (e.getSource() == easyOption) {
				frame.setContentPane(createBoard("EASY", rows, columns, mines));
			} else if (e.getSource() == mediumOption) {
				frame.setContentPane(createBoard("MEDIUM", rows, columns, mines));
			} else if (e.getSource() == hardOption) {
				frame.setContentPane(createBoard("HARD", rows, columns, mines));
			} else if (e.getSource() == resetButton) {
				board.reset();
				gameOver = false;
				frame.setTitle("Minesweeper");
				for (int i = 0; i < boardButtons.length; i++) {
					for (int j = 0; j < boardButtons[0].length; j++) {
						boardButtons[i][j].setBackground(defaultBg);
						boardButtons[i][j].setText("");
					}
				}
				resetButton.setIcon(new ImageIcon(Class.class.getResource("/resources/sun.jpg")));
				startTime = LocalDateTime.now();
				timer = new Timer(500, e1 -> {
					getTimer();
				});
				timer.start();
			}
			// When any board button is clicked
			if (gameOver == false) {
				for (int i = 0; i < board.getHeight(); i++) {
					for (int j = 0; j < board.getWidth(); j++) {
						if (e.getSource() == boardButtons[i][j] && !boardButtons[i][j].getText().equals(flag)) {

							if (board.isPiece(i, j, board.mine)) {
								gameOver = true;
								revealMines();
								frame.setTitle("Minesweeper: *** YOU LOSE ***");
								boardButtons[i][j].setBackground(Color.RED);
								JOptionPane.showMessageDialog(frame, "Bad Luck. You lose.");
								resetButton.setIcon(new ImageIcon(Class.class.getResource("/resources/cry.jpg")));
								timer.stop();
							} else {
								reveal(i, j);
								if (isWinner()) {
									gameOver = true;
									frame.setTitle("Minesweeper: *** YOU WIN ***");
									JOptionPane.showMessageDialog(frame, "Congratulations! You win!");
									timer.stop();
								}
							}
						}
					}
				}
			}
		}
	}


	// right clicked, flag
	public class MyMouseListener implements MouseListener {
		public void mousePressed(MouseEvent e) {
			if (gameOver == false) {
				for (int i = 0; i < boardButtons.length; i++) {
					for (int j = 0; j < boardButtons[0].length; j++) {
						String buttonText = boardButtons[i][j].getText();
						if (e.getButton() == 3 && e.getSource() == boardButtons[i][j]) {
							if (buttonText.equals("") || buttonText.equals(flag)) {
								setFlag(i, j);
							}
						}
					}
				}
			}
		}

		public void mouseClicked(MouseEvent e) {
		}

		public void mouseEntered(MouseEvent e) {
		}

		public void mouseExited(MouseEvent e) {
		}

		public void mouseReleased(MouseEvent e) {
		}

	}

	public static void main(String[] args) {
		new MinesweeperGUI("", rows, columns, mines);
	}

}
